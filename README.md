# GEDCOM

GEDCOM is an acronym for GEnealogical Data COMmunication and it
provides a flexible, uniform format for exchanging computerized
genealogical data.

[![build status](https://gitlab.com/ahmedcharles/gedcom-core/badges/main/pipeline.svg)
](https://gitlab.com/ahmedcharles/gedcom-core/commits/main)
[![Crates.io](https://img.shields.io/crates/v/gedcom-core.svg)
](https://crates.io/crates/gedcom-core)

## Usage

Add this to your `Cargo.toml` (crates.io):

```toml
[dependencies]
gedcom-core = "0.0.1"
```

or (git):

```toml
[dependencies]
gedcom-core = { git = "https://gitlab.com/ahmedcharles/gedcom-core" }
```

and this to your crate root:

```rust
use gedcom_core;
```

Get the latest version from [GitLab](https://gitlab.com/ahmedcharles/gedcom-core).

```
   Copyright 2017 Ahmed Charles <acharles@outlook.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
