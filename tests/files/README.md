The following are valid GEDCOM files:

1. complete.ged          - 'complete tag file', notes in the file.
2. sample555-utf8.ged    - GEDCOM 5.5.5 sample (UTF-8).
3. sample555-utf16le.ged - GEDCOM 5.5.5 sample (UTF-16 Big Endian).
4. sample555-utf16be.ged - GEDCOM 5.5.5 sample (UTF-16 Big Endian).
5. remarr.ged            - Remarriage example GEDCOM file.
6. ssmarr.ged            - Same-sex marriage example GEDCOM file.
7. minimal555.ged        - The minimal GEDCOM 5.5.5 file.
8. married1200.ged       - One man with 1200 wives.
9. children1200.ged      - One woman with 1200 children.
10. siblings1200.ged     - One man with 1200 wives, one child with each.
11. long26cc.ged         - 26 individuals with increasingly longer names.
