// Copyright 2017-2021 Ahmed Charles <acharles@outlook.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![deny(warnings)]
#![deny(absolute_paths_not_starting_with_crate)]
#![deny(deprecated_in_future)]
#![deny(elided_lifetimes_in_paths)]
#![deny(explicit_outlives_requirements)]
#![deny(keyword_idents)]
#![deny(macro_use_extern_crate)]
#![deny(meta_variable_misuse)]
#![deny(missing_abi)]
#![deny(missing_copy_implementations)]
#![deny(missing_debug_implementations)]
#![deny(missing_docs)]
#![deny(non_ascii_idents)]
#![deny(noop_method_call)]
#![deny(pointer_structural_match)]
#![deny(rust_2021_incompatible_closure_captures)]
#![deny(rust_2021_incompatible_or_patterns)]
#![deny(rust_2021_prefixes_incompatible_syntax)]
#![deny(rust_2021_prelude_collisions)]
#![deny(semicolon_in_expressions_from_macros)]
#![deny(single_use_lifetimes)]
#![deny(trivial_casts)]
#![deny(trivial_numeric_casts)]
#![deny(unreachable_pub)]
#![deny(unsafe_code)]
#![deny(unsafe_op_in_unsafe_fn)]
#![deny(unused_crate_dependencies)]
#![deny(unused_extern_crates)]
#![deny(unused_import_braces)]
#![deny(unused_lifetimes)]
#![deny(unused_qualifications)]
#![deny(unused_results)]
#![deny(variant_size_differences)]

use nom as _;
use serde as _;
use serde_json as _;
use smallvec as _;

// complete.ged          - 'complete tag file', notes in the file.
const COMPLETE: &str = include_str!("files/complete.ged");

#[test]
fn test_complete() {
    assert_eq!(63526, COMPLETE.len());
    let (_, l) = gedcom_core::data::lines(COMPLETE).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("complete", l);
    });
}

// sample555-utf8.ged    - GEDCOM 5.5.5 sample (UTF-8).
const SAMPLE_UTF8: &str = include_str!("files/sample555-utf8.ged");

#[test]
fn test_sample_utf8() {
    assert_eq!(1986, SAMPLE_UTF8.len());
    let (_, l) = gedcom_core::data::lines(SAMPLE_UTF8).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("sample_utf8", l);
    });
}

// sample555-utf16le.ged - GEDCOM 5.5.5 sample (UTF-16 Big Endian).
const SAMPLE_UTF16LE: &[u8] = include_bytes!("files/sample555-utf16le.ged");

#[test]
fn test_sample_utf16le() {
    assert_eq!(3972, SAMPLE_UTF16LE.len());
    let (enc, _) = encoding_rs::Encoding::for_bom(SAMPLE_UTF16LE).unwrap();
    let input = enc
        .decode_without_bom_handling_and_without_replacement(SAMPLE_UTF16LE)
        .unwrap();
    let (_, l) = gedcom_core::data::lines(&input).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("sample_utf16le", l);
    });
}

// sample555-utf16be.ged - GEDCOM 5.5.5 sample (UTF-16 Big Endian).
const SAMPLE_UTF16BE: &[u8] = include_bytes!("files/sample555-utf16be.ged");

#[test]
fn test_sample_utf16be() {
    assert_eq!(3972, SAMPLE_UTF16BE.len());
    let (enc, _) = encoding_rs::Encoding::for_bom(SAMPLE_UTF16BE).unwrap();
    let input = enc
        .decode_without_bom_handling_and_without_replacement(SAMPLE_UTF16BE)
        .unwrap();
    let (_, l) = gedcom_core::data::lines(&input).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("sample_utf16be", l);
    });
}

// remarr.ged            - Remarriage example GEDCOM file.
const REMARR: &str = include_str!("files/remarr.ged");

#[test]
fn test_remarr() {
    assert_eq!(1233, REMARR.len());
    let (_, l) = gedcom_core::data::lines(REMARR).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("remarr", l);
    });
}

// ssmarr.ged            - Same-sex marriage example GEDCOM file.
const SSMARR: &str = include_str!("files/ssmarr.ged");

#[test]
fn test_ssmarr() {
    assert_eq!(864, SSMARR.len());
    let (_, l) = gedcom_core::data::lines(SSMARR).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("ssmarr", l);
    });
}

// minimal555.ged        - The minimal GEDCOM 5.5.5 file.
const MINIMAL: &str = include_str!("files/minimal555.ged");

#[test]
fn test_minimal() {
    assert_eq!(132, MINIMAL.len());
    let (_, l) = gedcom_core::data::lines(MINIMAL).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("minimal", l);
    });
}

// married1200.ged       - One man with 1200 wives.
const MARRIED1200: &str = include_str!("files/married1200.ged");

#[test]
fn test_married1200() {
    assert_eq!(220057, MARRIED1200.len());
    let (_, l) = gedcom_core::data::lines(MARRIED1200).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("married1200", l);
    });
}

// children1200.ged      - One woman with 1200 children.
const CHILDREN1200: &str = include_str!("files/children1200.ged");

#[test]
fn test_children1200() {
    assert_eq!(169419, CHILDREN1200.len());
    let (_, l) = gedcom_core::data::lines(CHILDREN1200).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("children1200", l);
    });
}

// siblings1200.ged     - One man with 1200 wives, one child with each.
const SIBLINGS1200: &str = include_str!("files/siblings1200.ged");

#[test]
fn test_siblings1200() {
    assert_eq!(399500, SIBLINGS1200.len());
    let (_, l) = gedcom_core::data::lines(SIBLINGS1200).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("siblings1200", l);
    });
}

// long26cc.ged         - 26 individuals with increasingly longer names.
const LONG26CC: &str = include_str!("files/long26cc.ged");

#[test]
fn test_long26cc() {
    assert_eq!(9094, LONG26CC.len());
    let (_, l) = gedcom_core::data::lines(LONG26CC).unwrap();
    insta::with_settings!({snapshot_path => "files"}, {
        insta::assert_json_snapshot!("long26cc", l);
    });
}
