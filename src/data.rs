// Copyright 2021 Ahmed Charles <acharles@outlook.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! The core GEDCOM data representation language may be used to represent
//! any form of structured information, not just genealogical data, using
//! a sequential stream of characters.

use std::fmt;
use std::num::{NonZeroU8, NonZeroUsize};
use std::ops::{RangeFrom, RangeTo};

use nom::{
    error::{make_error, ErrorKind, ParseError},
    IResult, Needed,
};
use nom::{
    AsChar, Compare, CompareResult, ExtendInto, InputIter, InputLength, InputTake,
    InputTakeAtPosition, Offset, Slice,
};
use serde::{ser::SerializeSeq, Serialize, Serializer};
use smallvec::SmallVec;

#[derive(Debug)]
enum TextEsc<'a> {
    Text(&'a str),
    Esc(&'a str),
}

impl<'a> ExtendInto for TextEsc<'a> {
    type Item = char;
    type Extender = ItemsInner<'a>;
    fn new_builder(&self) -> Self::Extender {
        ItemsInner {
            data: SmallVec::new(),
        }
    }
    fn extend_into(&self, acc: &mut Self::Extender) {
        acc.data.push(match self {
            TextEsc::Text(t) => TextEsc::Text(t),
            TextEsc::Esc(e) => TextEsc::Esc(e),
        });
    }
}

/// Represents an efficient, extendable string.
#[allow(single_use_lifetimes)]
#[derive(Debug, Eq, PartialEq, Serialize)]
pub struct Item<'a>(ItemsInner<'a>);

#[derive(Debug)]
struct ItemsInner<'a> {
    data: SmallVec<[TextEsc<'a>; 1]>,
}

fn map_item_iter<'a>(
    item: &TextEsc<'a>,
) -> (
    Option<NonZeroU8>,
    std::slice::Iter<'a, u8>,
    Option<NonZeroU8>,
) {
    match item {
        TextEsc::Text(t) => (None, t.as_bytes().iter(), None),
        TextEsc::Esc(t) => (
            NonZeroU8::new(0xFF),
            t.as_bytes().iter(),
            NonZeroU8::new(0xFF),
        ),
    }
}

impl ItemsInner<'_> {
    fn bytes(&self) -> Bytes<'_> {
        let mut item_iter = self.data.iter();
        let str_iter = item_iter.next().map(map_item_iter);
        Bytes {
            item_iter,
            str_iter,
        }
    }
    fn len(&self) -> usize {
        let mut sum = 0;
        let mut esc = false;
        for item in &self.data {
            match item {
                TextEsc::Text(t) => {
                    if esc {
                        sum += 1;
                        esc = false;
                    }
                    sum += t.len()
                }
                TextEsc::Esc(t) => {
                    esc = true;
                    sum += 2 + t.len() + 1
                }
            }
        }
        sum
    }
}

struct Bytes<'a> {
    str_iter: Option<(
        Option<NonZeroU8>,
        std::slice::Iter<'a, u8>,
        Option<NonZeroU8>,
    )>,
    item_iter: std::slice::Iter<'a, TextEsc<'a>>,
}

impl Iterator for Bytes<'_> {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(ref mut str_iter) = self.str_iter {
            if let Some(b) = str_iter.0.take() {
                return Some(b.into());
            }
            if let Some(b) = str_iter.1.next() {
                return Some(*b);
            }
            if let Some(b) = str_iter.2.take() {
                return Some(b.into());
            }
            self.str_iter = self.item_iter.next().map(map_item_iter);
        }
        None
    }
}

impl Eq for ItemsInner<'_> {}

impl<'a> From<&'a str> for ItemsInner<'a> {
    fn from(s: &'a str) -> ItemsInner<'a> {
        let mut data = SmallVec::new();
        data.push(TextEsc::Text(s));
        ItemsInner { data }
    }
}

impl PartialEq for ItemsInner<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.bytes().eq(other.bytes())
    }
}

struct TextSlice<'a>(&'a [TextEsc<'a>]);

impl fmt::Display for TextSlice<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for item in self.0 {
            match item {
                TextEsc::Text(t) => f.write_str(t)?,
                TextEsc::Esc(_) => unreachable!(),
            }
        }
        Ok(())
    }
}

impl Serialize for TextSlice<'_> {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.collect_str(self)
    }
}

impl Serialize for ItemsInner<'_> {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        let mut seq = serializer.serialize_seq(None)?;
        let mut from = 0;
        for (i, item) in self.data.iter().enumerate() {
            if let TextEsc::Esc(t) = item {
                if from != i {
                    seq.serialize_element(&TextSlice(&self.data[from..i]))?;
                }
                seq.serialize_element(t)?;
                from = i + 1;
            }
        }
        if from != self.data.len() {
            seq.serialize_element(&TextSlice(&self.data[from..]))?;
        }
        seq.end()
    }
}

#[derive(Clone, Copy)]
struct Str<'a>(&'a str);

impl<'a> Compare<&'a str> for Str<'_> {
    fn compare(&self, t: &'a str) -> CompareResult {
        self.0.compare(t)
    }
    fn compare_no_case(&self, t: &'a str) -> CompareResult {
        self.0.compare_no_case(t)
    }
}

impl<'a> ExtendInto for Str<'a> {
    type Item = char;
    type Extender = ItemsInner<'a>;
    fn new_builder(&self) -> Self::Extender {
        ItemsInner {
            data: SmallVec::new(),
        }
    }
    fn extend_into(&self, acc: &mut Self::Extender) {
        acc.data.push(TextEsc::Text(self.0));
    }
}

impl<'a> InputIter for Str<'a> {
    type Item = char;
    type Iter = std::str::CharIndices<'a>;
    type IterElem = std::str::Chars<'a>;
    fn iter_indices(&self) -> Self::Iter {
        self.0.iter_indices()
    }
    fn iter_elements(&self) -> Self::IterElem {
        self.0.iter_elements()
    }
    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        self.0.position(predicate)
    }
    fn slice_index(&self, count: usize) -> Result<usize, Needed> {
        self.0.slice_index(count)
    }
}

impl InputLength for Str<'_> {
    fn input_len(&self) -> usize {
        self.0.input_len()
    }
}

impl InputTake for Str<'_> {
    fn take(&self, count: usize) -> Self {
        Str(self.0.take(count))
    }
    fn take_split(&self, count: usize) -> (Self, Self) {
        let (a, b) = self.0.take_split(count);
        (Str(a), Str(b))
    }
}

impl InputTakeAtPosition for Str<'_> {
    type Item = char;
    fn split_at_position<P, E: ParseError<Self>>(&self, predicate: P) -> IResult<Self, Self, E>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.0.find(predicate) {
            Some(i) => Ok((Str(&self.0[i..]), Str(&self.0[..i]))),
            None => Err(nom::Err::Incomplete(nom::Needed::Size(
                NonZeroUsize::new(1).unwrap(),
            ))),
        }
    }
    fn split_at_position1<P, E: ParseError<Self>>(
        &self,
        predicate: P,
        e: ErrorKind,
    ) -> IResult<Self, Self, E>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.0.find(predicate) {
            Some(0) => Err(nom::Err::Error(E::from_error_kind(*self, e))),
            Some(i) => Ok((Str(&self.0[i..]), Str(&self.0[..i]))),
            None => Err(nom::Err::Incomplete(nom::Needed::Size(
                NonZeroUsize::new(1).unwrap(),
            ))),
        }
    }
    fn split_at_position_complete<P, E: ParseError<Self>>(
        &self,
        predicate: P,
    ) -> IResult<Self, Self, E>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.0.find(predicate) {
            Some(i) => Ok((Str(&self.0[i..]), Str(&self.0[..i]))),
            None => Ok(self.take_split(self.input_len())),
        }
    }
    fn split_at_position1_complete<P, E: ParseError<Self>>(
        &self,
        predicate: P,
        e: ErrorKind,
    ) -> IResult<Self, Self, E>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.0.find(predicate) {
            Some(0) => Err(nom::Err::Error(E::from_error_kind(*self, e))),
            Some(i) => Ok((Str(&self.0[i..]), Str(&self.0[..i]))),
            None => {
                if self.0.is_empty() {
                    Err(nom::Err::Error(E::from_error_kind(*self, e)))
                } else {
                    Ok(self.take_split(self.input_len()))
                }
            }
        }
    }
}

impl Offset for Str<'_> {
    fn offset(&self, second: &Self) -> usize {
        self.0.offset(second.0)
    }
}

impl Slice<RangeFrom<usize>> for Str<'_> {
    fn slice(&self, range: RangeFrom<usize>) -> Self {
        Str(self.0.slice(range))
    }
}

impl Slice<RangeTo<usize>> for Str<'_> {
    fn slice(&self, range: RangeTo<usize>) -> Self {
        Str(self.0.slice(range))
    }
}

fn escaped_transform_<'a, F: 'a, G: 'a>(
    normal: F,
    control_char: char,
    transform: G,
) -> impl FnMut(&'a str) -> IResult<&'a str, ItemsInner<'a>> + 'a
where
    F: FnMut(Str<'a>) -> IResult<Str<'a>, Str<'a>>,
    G: FnMut(Str<'a>) -> IResult<Str<'a>, TextEsc<'a>>,
{
    let mut e = nom::bytes::complete::escaped_transform(normal, control_char, transform);
    move |i: &str| {
        e(Str(i))
            .map(|(i, o)| (i.0, o))
            .map_err(|e| e.map_input(|i| i.0))
    }
}

fn one_of_<I, F, E: ParseError<I>>(f: F) -> impl Fn(I) -> IResult<I, I, E>
where
    I: Slice<RangeFrom<usize>> + Slice<RangeTo<usize>> + InputIter,
    <I as InputIter>::Item: AsChar + Copy,
    F: Fn(<I as InputIter>::Item) -> bool,
{
    move |i: I| match (i).iter_elements().next().map(|c| (c, f(c))) {
        Some((c, true)) => Ok((i.slice(c.len()..), i.slice(..c.len()))),
        _ => Err(nom::Err::Error(E::from_error_kind(i, ErrorKind::OneOf))),
    }
}

/// Represents a line value, either a pointer or item.
#[allow(single_use_lifetimes)]
#[derive(Debug, Eq, PartialEq, Serialize)]
pub enum Value<'a> {
    /// Represents a pointer to another record.
    Pointer(&'a str),
    /// Represents an actual value.
    Item(Item<'a>),
}

/// Represents an entire line or record in the GEDCOM data format.
#[allow(single_use_lifetimes)]
#[derive(Debug, Eq, PartialEq, Serialize)]
pub struct Line<'a> {
    level: u8,
    xref: Option<&'a str>,
    tag: &'a str,
    value: Option<Value<'a>>,
}

impl<'a> Line<'a> {
    /// The level of this record.
    pub fn level(&self) -> u8 {
        self.level
    }
    /// The optional cross-reference identifier for this record.
    pub fn xref(&self) -> Option<&'a str> {
        self.xref
    }
    /// The tag for this record.
    pub fn tag(&self) -> &'a str {
        self.tag
    }
    /// The optional value for this record.
    pub fn value(&self) -> Option<&Value<'a>> {
        self.value.as_ref()
    }
    fn len(&self) -> usize {
        let level_len = if self.level < 10 { 1 } else { 2 };
        let xref_len = if let Some(xref) = self.xref {
            1 + 2 + xref.len()
        } else {
            0
        };
        let value_len = match self.value {
            Some(Value::Pointer(p)) => 1 + 2 + p.len(),
            Some(Value::Item(ref text)) if text.0.len() == 0 => unreachable!(),
            Some(Value::Item(ref text)) => {
                1 + text.0.len() + text.0.bytes().filter(|&c| c == b'@').count()
            }
            None => 0,
        };
        level_len + xref_len + 1 + self.tag.len() + value_len
    }
}

fn line<'a>(terminator: &'a str) -> impl Fn(&'a str) -> IResult<&'a str, Line<'a>> {
    move |input: &str| {
        use nom::branch::alt;
        use nom::bytes::complete::{tag, take_while, take_while1};
        use nom::character::complete::{alphanumeric1, one_of};
        use nom::combinator::{map, map_opt, opt, peek, recognize, verify};
        use nom::sequence::{delimited, preceded, terminated, tuple};
        use nom::ParseTo;

        let delim_ = tag(" ");
        let alphanum_ = || one_of_(|ch: char| ch.is_ascii_alphanumeric());
        let alphanum_space_ = || take_while(|ch: char| ch.is_ascii_alphanumeric() || ch == ' ');
        let digit_ = take_while1(|ch: char| ch.is_ascii_digit());

        // [ digit | non_zero_digit + digit ]
        let l_digit_ = alt((tag("0"), preceded(peek(one_of("123456789")), digit_)));
        let level_ = verify(map_opt(l_digit_, |i: &str| i.parse_to()), |&o| o < 100);

        // [ alphanum | alphanum + identifier_string ]
        let identifier_string_ = || verify(alphanumeric1, |o: &str| o.len() <= 20);

        // U+0040 + identifier_string + U+0040
        let pointer_ = || delimited(tag("@"), identifier_string_(), tag("@"));

        // [ [ U+005F ] + alphanum | tag + alphanum ]
        let tag_ = verify(
            recognize(preceded(opt(tag("_")), alphanumeric1)),
            |o: &str| o.len() <= 31,
        );

        // [ alphanum | escape_text + alphanum | escape_text + space ]
        let escape_text_ = || recognize(preceded(alphanum_(), alphanum_space_()));

        // U+0040 + U+0023 + escape_text + U+0040
        let escape_ = || {
            map(
                delimited(
                    tag("#"),
                    escape_text_(),
                    terminated(tag("@"), opt(tag(" "))),
                ),
                |o: Str<'_>| TextEsc::Esc(o.0),
            )
        };

        // [ line_char | line_text + line_char ]
        let line_text_ = || {
            escaped_transform_(
                one_of_(|ch: char| {
                    !matches!(ch,
                        // disallowed: U+0000 - U+001F, except U+0009 = most C0 control characters
                        '\u{0000}'..='\u{0008}' |
                        '\u{000A}'..='\u{001F}' |
                        // special: U+0040 + U+0040 = @@
                        '@' |
                        // disallowed: U+00FF = Delete character
                        '\u{00FF}'
                    )
                }),
                '@',
                alt((
                    map(one_of_(|ch: char| ch == '@'), |o: Str<'_>| {
                        TextEsc::Text(o.0)
                    }),
                    escape_(),
                )),
            )
        };

        // [ escape | line_text | escape + delim + line_text ]
        // Note: this is inaccurate, because dates allow text before escapes,
        // e.g. ABT @#FRENCH R@ 11 NIVO 6
        let line_item_ = || map(line_text_(), |t| Value::Item(Item(t)));

        // [ pointer | line_item ]
        let line_value_ = alt((map(pointer_(), Value::Pointer), line_item_()));

        // use the detected ending
        let terminator_ = tag(terminator);

        // level + [ delim + xref_ID ] + delim + tag + [ delim + line_value ] + terminator
        let opt_pointer_ = opt(preceded(tag(" "), pointer_()));
        let opt_line_value = opt(preceded(tag(" "), opt(line_value_)));
        let (input, (l, x, _, t, v, _)) = tuple((
            level_,
            opt_pointer_,
            delim_,
            tag_,
            opt_line_value,
            terminator_,
        ))(input)?;
        Ok((
            input,
            Line {
                level: l,
                xref: x,
                tag: t,
                value: v.flatten(),
            },
        ))
    }
}

fn verify_lines<'a>(
    (i, (terminator, ls)): (&'a str, (&'a str, Vec<Line<'a>>)),
) -> IResult<&'a str, Vec<Line<'a>>> {
    let mut records = std::collections::BTreeSet::new();
    let mut last: Option<&Line<'_>> = None;
    for l in &ls {
        if l.len() + terminator.len() > 255 {
            return Err(nom::Err::Failure(make_error(i, ErrorKind::Verify)));
        }
        let last_plus_1 = last.map(|r| r.level + 1).unwrap_or(0);
        if l.level > last_plus_1 {
            return Err(nom::Err::Failure(make_error(i, ErrorKind::Verify)));
        }
        let subrecord = l.level != 0 && l.level == last_plus_1;
        if !subrecord
            && !last
                .map(|r| r.tag == "CONT" || r.value.is_some())
                .unwrap_or(true)
        {
            return Err(nom::Err::Failure(make_error(i, ErrorKind::Verify)));
        }
        if let Some(xref) = l.xref {
            if !records.insert(xref) {
                return Err(nom::Err::Failure(make_error(i, ErrorKind::Verify)));
            }
        }
        last = Some(l);
    }
    for l in &ls {
        if let Some(Value::Pointer(p)) = l.value {
            if !records.contains(p) {
                return Err(nom::Err::Failure(make_error(i, ErrorKind::Verify)));
            }
        }
    }
    Ok((i, ls))
}

/// Parses a string (GEDCOM file content) into a sequence of `Line`s.
pub fn lines(input: &str) -> IResult<&'_ str, Vec<Line<'_>>> {
    use nom::branch::alt;
    use nom::bytes::complete::{tag, take_while};
    use nom::combinator::{all_consuming, flat_map, map, opt, peek, recognize};
    use nom::multi::many1;
    use nom::sequence::preceded;

    // [ carriage_return | line_feed | carriage_return + line_feed ]
    let terminator_ = alt((recognize(preceded(tag("\r"), opt(tag("\n")))), tag("\n")));

    let not_line_ending_ = take_while(|ch: char| ch != '\r' && ch != '\n');
    let find_terminator_ = peek(preceded(not_line_ending_, terminator_));
    all_consuming(preceded(
        tag("\u{FEFF}"),
        flat_map(find_terminator_, |i| map(many1(line(i)), move |o| (i, o))),
    ))(input)
    .and_then(verify_lines)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn terminators() {
        let expected_line = Line {
            level: 0,
            xref: None,
            tag: "HEAD",
            value: None,
        };
        let expected = ("", expected_line);
        assert_eq!(expected, line("\r")("0 HEAD\r").unwrap());
        assert_eq!(expected, line("\n")("0 HEAD\n").unwrap());
        assert_eq!(expected, line("\r\n")("0 HEAD\r\n").unwrap());
    }

    fn valid_case<'a>(input: &'a str, l: u8, x: Option<&'a str>, t: &'a str, v: Option<Value<'a>>) {
        let expected_line = Line {
            level: l,
            xref: x,
            tag: t,
            value: v,
        };
        let expected = ("", expected_line);
        assert_eq!(expected, line("\r\n")(input).unwrap());
        eprintln!("{}", input);
        let c = 2 + if input.ends_with(" \r\n") { 1 } else { 0 };
        assert_eq!(input.len(), line("\r\n")(input).unwrap().1.len() + c);
    }

    fn invalid_case(input: &str, len: usize) {
        let l = line("\r\n")(input);
        match l {
            Ok(v) => {
                eprintln!("{:?}", v);
                assert!(false);
            }
            Err(nom::Err::Incomplete(e)) => {
                eprintln!("{:?}", e);
                assert!(false);
            }
            Err(nom::Err::Failure(e)) => {
                eprintln!("{:?}", e);
                assert!(false);
            }
            Err(nom::Err::Error(e)) => {
                eprintln!("{:?}", e);
                assert_eq!(len, e.input.len());
            }
        }
    }

    #[test]
    fn tags() {
        valid_case("0 HEAD\r\n", 0, None, "HEAD", None);
        let upper = "0 ABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n";
        valid_case(upper, 0, None, &upper[2..28], None);
        let lower = "0 abcdefghijklmnopqrstuvwxyz\r\n";
        valid_case(lower, 0, None, &lower[2..28], None);
        valid_case("0 _0123456789\r\n", 0, None, "_0123456789", None);
        valid_case("0 ADDR \r\n", 0, None, "ADDR", None);
        let max = "99 @N1234567890123456789@ ABCDEFGHIJKLMNOPQRSTUVWXYZ01234 \r\n";
        valid_case(max, 99, Some(&max[4..24]), &max[26..57], None);
    }

    #[test]
    fn levels() {
        for i in 0..100 {
            let l = format!("{} HEAD\r\n", i);
            valid_case(&l, i, None, "HEAD", None);
        }
    }

    #[test]
    fn simple_value() {
        let v = Some(Value::Item(Item("UTF-8".into())));
        valid_case("1 CHAR UTF-8\r\n", 1, None, "CHAR", v);
    }

    #[test]
    fn simple_xref() {
        valid_case("0 @N1@ NOTE\r\n", 0, Some("N1"), "NOTE", None);
    }

    #[test]
    fn simple_pointer() {
        let v = Some(Value::Pointer("N1234567890123456789"));
        valid_case("1 NOTE @N1234567890123456789@\r\n", 1, None, "NOTE", v);
    }

    #[test]
    fn simple_note() {
        let v = Some(Value::Item(Item("foo".into())));
        valid_case("0 @N1@ NOTE foo\r\n", 0, Some("N1"), "NOTE", v);
    }

    #[test]
    fn escape_line_value() {
        let mut items = ItemsInner {
            data: SmallVec::new(),
        };
        Str("ABT ").extend_into(&mut items);
        TextEsc::Esc("DFRENCH R").extend_into(&mut items);
        Str("11 NIVO 6").extend_into(&mut items);
        let v = Some(Value::Item(Item(items)));
        valid_case("1 DATE ABT @#DFRENCH R@ 11 NIVO 6\r\n", 1, None, "DATE", v);
    }

    #[test]
    fn escape_at() {
        let v = Some(Value::Item(Item("foo@example.com".into())));
        valid_case("1 EMAIL foo@@example.com\r\n", 1, None, "EMAIL", v);
        let v = Some(Value::Item(Item("@foo".into())));
        valid_case("1 NOTE @@foo\r\n", 1, None, "NOTE", v);
    }

    #[test]
    fn invalid_tags() {
        invalid_case("0 __HEAD\r\n", 7);
        invalid_case("0 ABCDEFGHIJKLMNOPQRSTUVWXYZ012345\r\n", 34);
    }

    #[test]
    fn invalid_levels() {
        invalid_case("01 HEAD\r\n", 8);
        invalid_case("100 HEAD\r\n", 10);

        assert!(lines("\u{FEFF}1 HEAD\r\n").is_err());
        assert!(lines("\u{FEFF}0 HEAD\r\n2 VERS 5.5.5\r\n").is_err());
    }

    #[test]
    fn invalid_pointer() {
        invalid_case("0 @N01234567890123456789@ NOTE foo\r\n", 34);
        invalid_case("0 NOTE @N01234567890123456789@\r\n", 25);
    }

    #[test]
    fn leading_whitespace() {
        let expected_line = Line {
            level: 0,
            xref: None,
            tag: "HEAD",
            value: None,
        };
        let expected = ("\r", expected_line);
        assert_eq!(expected, line("\n")("0 HEAD\n\r").unwrap());

        invalid_case(" 0 HEAD\r\n", 9);
        invalid_case("\t0 HEAD\r\n", 9);
        invalid_case("\r\n0 HEAD\r\n", 10);
        invalid_case("\n0 HEAD\r\n", 9);
        invalid_case("\r0 HEAD\r\n", 9);
        invalid_case("\n\r0 HEAD\r\n", 10);
    }

    #[test]
    fn multiple_lines() {
        let expected_lines = vec![
            Line {
                level: 0,
                xref: None,
                tag: "HEAD",
                value: None,
            },
            Line {
                level: 1,
                xref: None,
                tag: "GEDC",
                value: None,
            },
            Line {
                level: 2,
                xref: None,
                tag: "VERS",
                value: Some(Value::Item(Item("5.5.5".into()))),
            },
            Line {
                level: 0,
                xref: None,
                tag: "TRLR",
                value: None,
            },
        ];
        let cr = "\u{FEFF}0 HEAD\r1 GEDC\r2 VERS 5.5.5\r0 TRLR\r";
        assert_eq!(expected_lines, lines(cr).unwrap().1);
        let lf = "\u{FEFF}0 HEAD\n1 GEDC\n2 VERS 5.5.5\n0 TRLR\n";
        assert_eq!(expected_lines, lines(lf).unwrap().1);
        let crlf = "\u{FEFF}0 HEAD\r\n1 GEDC\r\n2 VERS 5.5.5\r\n0 TRLR\r\n";
        assert_eq!(expected_lines, lines(crlf).unwrap().1);
    }

    #[test]
    fn invalid_xrefs() {
        assert!(lines("\u{FEFF}0 @N1@ NOTE\n0 NOTE @N2@\n").is_err());
        assert!(lines("\u{FEFF}0 @N1@ NOTE\n0 @N1@ NOTE\n").is_err());
    }

    #[test]
    fn invalid_terminators() {
        assert!(lines("\u{FEFF}0 HEAD\r0 TRLR\n").is_err());
        assert!(lines("\u{FEFF}0 HEAD\n\r0 TRLR\n\r").is_err());
    }

    fn valid_items(items: &ItemsInner<'_>, len: usize, bytes: &[u8], json: &str) {
        assert_eq!(len, items.len());
        assert_eq!(bytes, &*items.bytes().collect::<Vec<_>>());
        assert_eq!(json, &serde_json::to_string(items).unwrap());
    }

    #[test]
    fn items() {
        let mut items = ItemsInner {
            data: SmallVec::new(),
        };
        Str("hello").extend_into(&mut items);
        Str(" ").extend_into(&mut items);
        Str("world").extend_into(&mut items);
        Str("!").extend_into(&mut items);
        valid_items(&items, 12, b"hello world!", r#"["hello world!"]"#);
        items.data.clear();
        TextEsc::Esc("hello").extend_into(&mut items);
        valid_items(&items, 8, b"\xFFhello\xFF", r#"["hello"]"#);
        items.data.clear();
        Str("ABT ").extend_into(&mut items);
        TextEsc::Esc("DFRENCH R").extend_into(&mut items);
        Str("11 NIVO 6").extend_into(&mut items);
        valid_items(
            &items,
            26,
            b"ABT \xFFDFRENCH R\xFF11 NIVO 6",
            r#"["ABT ","DFRENCH R","11 NIVO 6"]"#,
        );
        items.data.clear();
        TextEsc::Esc("DFRENCH R").extend_into(&mut items);
        Str("11 NIVO 6").extend_into(&mut items);
        valid_items(
            &items,
            22,
            b"\xFFDFRENCH R\xFF11 NIVO 6",
            r#"["DFRENCH R","11 NIVO 6"]"#,
        );
    }
}
